#include <stdio.h>

static inline void swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

static inline int partition(int* arr, int low, int high)
{
	// Smaller element index
	int i = low;

	// Loop through partition and swap smaller element with current
	for (int j = low; j < high; j++)
		if (arr[j] < arr[high])
			swap(arr + i++, arr + j);

	// Swap the smallest element and the pivot and return the partition divider index
	swap(arr + i, arr + high);
	return i;
}

void quicksort(int* arr, int low, int high)
{
	// If the partition size is zero return
	if (low >= high)
		return;

	// Item in middle of the two partitions
	// mid is already in the right place
	int mid = partition(arr, low, high);
	
	// Sort both parititon/halfs
	quicksort(arr, low, mid - 1);
	quicksort(arr, mid + 1, high);
}

void print_array(int* arr, int length)
{
	if (length > 0)
		printf("%d", arr[0]);
	for (int i = 1; i < length; i++)
		printf(", %d", arr[i]);
	printf("\n");
}

int array [] = {3, 2, 1, 4, 27, 72, 1, 92};

int main(int argc, char** argv)
{
	quicksort(array, 0, sizeof(array)/sizeof(array[0]));
	print_array(array, sizeof(array)/sizeof(array[0]));
}
